This is a DOS port of Rossies Classic

Made with LoveDOS: https://github.com/rxi/lovedos

Made for MS-DOS Game Jam #2: https://itch.io/jam/dos-game-jam-2

Information on how to build the program: https://github.com/rxi/lovedos/blob/master/doc/packaging.md