require("scripts.KRN")
require("scripts.ALPHA")
require("scripts.BRICK")
require("scripts.YUNKI")
require("scripts.SB")
require("scripts.SLIDES")

local gameMode = 0
local language = 1
local level = 0
local boss_mode = false
local kCount = 0
KREQUIRED = 50
local MAX_KEQUIRED = 200
local BG
kCooler = 1.5

alpha = {}
alpha = Alpha:create()

kruncanites = {}
kruncanites[0] = Kruncanite:create()
slides = {}

brick = {}
brick = Brick:create()

speechBubble = {}
speechBubble = SB:create()

yunki = Yunki:create()

local slideSprites = {}

local slimeTimer = 0
local SLIME_COOLER = 0.5
local slimeX = 0
local slimeY = 59

local currentSlide = -1

function love.load()
  slideAdvanceSound = love.audio.newSource("Sound/SLIDE.wav")
  -- music = love.audio.newSource("Sound/MUSIC.WAV")
  -- music:setLooping(true)
  -- music:play()
  loadScene(gameMode)
end

function love.draw()
  if gameMode == 2 then
    love.graphics.draw(BG, 0, 0)

    if kCount < KREQUIRED then
      for i = 0, level, 1
      do
        if kruncanites[i].state > 0 and kruncanites[i].state < 3 then
          love.graphics.draw(slideSprites[kruncanites[i].frame], kruncanites[i].position, 59)
        end
      end
    elseif boss_mode == true then
        love.graphics.draw(slideSprites[yunki.frame], yunki.posX - (slideSprites[0]:getWidth() / 2), yunki.posY - (slideSprites[0]:getHeight() / 2))
    end

    if slimeTimer > 0 then
      love.graphics.draw(slimeSprite, slimeX - (slimeSprite:getWidth() / 2), slimeY)
      slimeY = slimeY + (love.timer.getDelta() * 200)
      slimeTimer = slimeTimer - love.timer.getDelta()
    end

      love.graphics.draw(alpha.anim[alpha.frame], alpha.position, alpha.y[alpha.frame])
      
      if brick.available == false then
        love.graphics.draw(brickSprite, brick.positionx, brick.positiony)
      end

      if speechBubble.available == false then
        love.graphics.setColor(0, 0, 0)
        love.graphics.draw(speechSprite, speechBubble.positionx, speechBubble.positiony, speechBubble.flipped)
        love.graphics.print(text1[speechBubble.currentQuote], (speechBubble.positionx + speechSprite:getWidth() / 2) - (love.graphics.getFont():getWidth(text1[speechBubble.currentQuote]) / 2), speechBubble.positiony + 2)
        love.graphics.print(text2[speechBubble.currentQuote], (speechBubble.positionx + speechSprite:getWidth() / 2) - (love.graphics.getFont():getWidth(text2[speechBubble.currentQuote]) / 2), (speechBubble.positiony + speechSprite:getHeight() / 2) - (love.graphics.getFont():getHeight() / 2) - 2)
        love.graphics.print(text3[speechBubble.currentQuote], (speechBubble.positionx + speechSprite:getWidth() / 2) - (love.graphics.getFont():getWidth(text3[speechBubble.currentQuote]) / 2), (speechBubble.positiony + speechSprite:getHeight()) - love.graphics.getFont():getHeight() - 10)
        -- love.graphics.print("Bawbag!", (speechBubble.positionx + speechSprite:getWidth() / 2) - (love.graphics.getFont():getWidth("Bawbag!") / 2), (speechBubble.positiony + speechSprite:getHeight() / 2) - (love.graphics.getFont():getHeight() / 2))
        -- love.graphics.print("Manky!", (speechBubble.positionx + speechSprite:getWidth() / 2) - (love.graphics.getFont():getWidth("Manky!") / 2), (speechBubble.positiony + speechSprite:getHeight() / 2) - (love.graphics.getFont():getHeight() / 2))
        love.graphics.setColor()
      end

      -- love.graphics.setColor(0.5, 0.5, 0.5)
      -- love.graphics.rectangle("fill", 4, 4, 110, 12)
      love.graphics.setColor(0, 0.5, 0)
      love.graphics.rectangle("fill", 4, 4, alpha.health, 10)
      love.graphics.setColor()
      if language == 0 then
        love.graphics.print("Alpha Halth: " .. alpha.health, 5, 5)
      else
        love.graphics.print("Alpha Health: " .. alpha.health, 5, 5)
      end

      if kCount < KREQUIRED then
        love.graphics.setColor(0, 0.5, 0)
        love.graphics.rectangle("fill", love.graphics.getWidth() - (KREQUIRED - kCount) - 5, 4, KREQUIRED - kCount, 10)
        love.graphics.setColor()
        if language == 0 then
          love.graphics.print("Kruncanite Coont: " .. kCount, love.graphics.getWidth() - love.graphics.getFont():getWidth("Kruncanite Coont: " .. kCount) - 5, 5)
        else
          love.graphics.print("Kruncanite Count: " .. kCount, love.graphics.getWidth() - love.graphics.getFont():getWidth("Kruncanite Count: " .. kCount) - 5, 5)
        end
      elseif boss_mode == true then
        love.graphics.setColor(0, 0.5, 0)
        love.graphics.rectangle("fill", love.graphics.getWidth() - yunki.health - 5, 4, yunki.health, 10)
        love.graphics.setColor()
        if language == 0 then
          love.graphics.print("Yunki Halth: " .. yunki.health, love.graphics.getWidth() - love.graphics.getFont():getWidth("Yunki Halth: " .. yunki.health) - 5, 5)
        else
          love.graphics.print("Yunki health: " .. yunki.health, love.graphics.getWidth() - love.graphics.getFont():getWidth("Yunki Health: " .. yunki.health) - 5, 5)
        end
        -- love.graphics.print(yunki.state, 5, 55)
      end

      if alpha.health <= 0 then
        if language == 0 then
          love.graphics.print("Ye'r Deid! Push R tae reset or Esc tae Git Oot", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Ye'r Deid! Push R tae reset or Esc tae Git Oot") / 2), 90)
        else
          love.graphics.print("You are Dead! Press R to reset or Esc to Quit", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("You are Dead! Press R to reset or Esc to Quit") / 2), 90)
        end
      end
      -- --Debug
      -- love.graphics.print(math.random(8), 5, 35)
      -- love.graphics.print(yunki.timer2, 5, 45)
      -- love.graphics.print(alpha.position, 5, 59)
      -- love.graphics.print((alpha.position - kruncanites[0].position) / 70, 5, 75)
      -- love.graphics.print(brick.velocityx / 70, 5, 85)
      

      -- love.graphics.print(kruncanites[0].position, 105, 15)
      -- love.graphics.print(alpha.position + alphaSprites[0]:getWidth(), 105, 25)
    elseif gameMode == 1 or gameMode == 3 then
      love.graphics.draw(slideSprites[slides[currentSlide].slide], (love.graphics.getWidth() / 2) - (slideSprites[slides[currentSlide].slide]:getWidth() / 2), 50 - (slideSprites[slides[currentSlide].slide]:getHeight() / 2))
      love.graphics.print(slides[currentSlide].text1, (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth(slides[currentSlide].text1) / 2), 100)
      love.graphics.print(slides[currentSlide].text2, (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth(slides[currentSlide].text2) / 2), 110)
      love.graphics.print(slides[currentSlide].text3, (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth(slides[currentSlide].text3) / 2), 120)
      love.graphics.print(slides[currentSlide].text4, (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth(slides[currentSlide].text4) / 2), 130)
      love.graphics.print(slides[currentSlide].text5, (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth(slides[currentSlide].text5) / 2), 140)
    elseif gameMode == 4 then
      love.graphics.print("THE END", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("THE END") / 2), 70)
      if language == 0 then
        if KREQUIRED >= MAX_KEQUIRED then
          love.graphics.print("G'ON YERSEL! YE'V 100%IT 'E GEMM!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("G'ON YERSEL! YE'V 100%IT 'E GEMM!") / 2), 40)
        else
          love.graphics.print("Want tae gie hit anither play?", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Want tae gie hit anither play?") / 2), 170)
        end
        love.graphics.print("Graphics an Story bi: Cobra! an Q", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Graphics an Story bi: Cobra! an Q") / 2), 90)
        love.graphics.print("Muisic bi: Q", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Muisic bi: Q") / 2), 100)
        love.graphics.print("Programmin bi: Cobra!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Programmin bi: Cobra!") / 2), 110)
        love.graphics.print("Soond FX tak fae FreeSound.org", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Soond FX tak fae FreeSound.org") / 2), 120)
        love.graphics.print("Made uisin the LOVEDOS ingine", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Made uisin the LOVEDOS ingine") / 2), 130)
        love.graphics.print("Ta fur playin!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Ta fur playin!") / 2), 150)
      else
        if KREQUIRED >= MAX_KEQUIRED then
          love.graphics.print("YOU DID IT! YOU HAVE BEATEN THE GAME 100%!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("YOU DID IT! YOU HAVE BEATEN THE GAME 100%!") / 2), 40)
        else
          love.graphics.print("How about another go?", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("How about another go?") / 2), 170)
        end
        love.graphics.print("Graphics and Story by: Cobra! and Q", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Graphics and Story by: Cobra! and Q") / 2), 90)
        love.graphics.print("Music by: Q", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Music by: Q") / 2), 100)
        love.graphics.print("Programming by: Cobra!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Programming by: Cobra!") / 2), 110)
        love.graphics.print("Sound FX taken from FreeSound.org", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Sound FX taken from FreeSound.org") / 2), 120)
        love.graphics.print("Made using the LOVEDOS engine", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Made using the LOVEDOS engine") / 2), 130)
        love.graphics.print("Thanks for playing!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Thanks for playing!") / 2), 150)
      end
    else
      love.graphics.print("Gamerstorm Presents:", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Gamerstorm Presents:") / 2), 20)
      love.graphics.draw(slideSprites[0], (love.graphics.getWidth() / 2) - (slideSprites[0]:getWidth() / 2), 30)
      if language == 0 then
        love.graphics.print("Nou on DOS!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Nou on DOS!") / 2), 80)
        love.graphics.print("Press L for English", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Press L for English") / 2), 140)
        love.graphics.print("Push Space tae Stairt", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Push Space tae Stairt") / 2), 150)
        love.graphics.print("Push Esc tae Git Oot", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Push Esc tae Git Oot") / 2), 160)
        love.graphics.print("Copyricht (c) RRTF, Licensit bi Gamerstorm", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Copyricht (c) RRTF, Licensit bi Gamerstorm") / 2), 190)
      else
        love.graphics.print("Now on DOS!", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Now on DOS!") / 2), 80)
        love.graphics.print("Push L fur Scots", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Push L fur Scots") / 2), 140)
        love.graphics.print("Press Space to Start", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Press Space to Start") / 2), 150)
        love.graphics.print("Press Esc to Quit", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Press Esc to Quit") / 2), 160)
        love.graphics.print("Copyright (c) RRTF, Licensed by Gamerstorm", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Copyright (c) RRTF, Licensed by Gamerstorm") / 2), 190)
      end
      love.graphics.print("Version 1.1", (love.graphics.getWidth() / 2) - (love.graphics.getFont():getWidth("Version 1.1") / 2), 180)
  end
  -- love.graphics.print(love.system.getMemUsage(), 5, 15)
end

function collided(x1, w1, x2, w2)
  return (x1 < x2 + w2 and x1 + w1 > x2)
end

function collide2d(x1, y1, w1, h1, x2, y2, w2, h2)
  return (x1 < x2 + w2 and x1 + w1 > x2 and y1 < y2 + h2 and y1 + h1 > y2)
end

function explode(x, y)
  explosionSound:stop()
  slimeSound:stop()
  explosionSound:play()
  slimeSound:play()

  slimeX = x
  slimeY = y
  slimeTimer = SLIME_COOLER
end

function love.update()
	function love.keypressed(key)
	  if key == "escape" then
		  love.event.quit()
    elseif key == "space" then
      if gameMode == 2 then
        if alpha.state ~= 2 and alpha.state > 0 then
          alpha.state = 2
          shotgunSound:stop()
          shotgunSound:play()
          if kCount < KREQUIRED then
            for i = 0, level, 1
            do
              if collided(alpha.position + 45, 10, kruncanites[i].position, slideSprites[0]:getWidth()) == true then
                kCount = kCount + 1
                explode(kruncanites[i].position + (slideSprites[kruncanites[i].frame]:getWidth() / 2), 59)
                kruncanites[i]:deid()
                if kCount % 30 == 0 then
                  level = level + 1
                  kruncanites[level] = Kruncanite:create()
                end
                if kCount >= KREQUIRED then
                  activateBoss()
                end
              end
            end
          else
            if collided(alpha.position + 45, 10, yunki.posX, slideSprites[0]:getWidth()) == true then
              yunki:hurt()
              explode(yunki.posX + (slideSprites[0]:getWidth() / 2), yunki.posY)
            end
          end
        end
      elseif gameMode == 1 then
        if advanceSlides(10) then
          resetGame()
        end
      elseif gameMode == 3 then
        if advanceSlides(1) then
          gameMode = 4
        end
      elseif gameMode == 4 then
        resetGame()
        if KREQUIRED < MAX_KEQUIRED then
          KREQUIRED = KREQUIRED + 50
        else
          love.event.quit()
        end
        if yunki_max_health < 200 then
          yunki_max_health = yunki_max_health + 50
          loadScene(0)
        end
      else
        currentSlide = -1
        advanceSlides(10)
        loadScene(1)
      end
    elseif key == "r" then
      if gameMode == 2 then
        resetGame()
      end
    elseif key == "l" then
      language = language + 1
      if language > 1 then
        language = 0
      end
    end
  end

  if gameMode == 2 then
    alpha:update()

    if brick.available == false then
      brick:move()
    end
    if speechBubble.available == false then
      speechBubble:process()
    end

    if kCount < KREQUIRED then
      for i = 0, level, 1
      do
        if checkPositions(i) then
          kruncanites[i]:spawn()
        end
        if kruncanites[i].state > 0 then
          kruncanites[i]:logic()
        else 
          kruncanites[i]:spawn()
        end
      end
    else
      if yunki.state >= 0 then
        yunki:update()
        if yunki.state == 0 then
          if yunki.timer2 % (death_cooler / 15) < 0.1 then
            explode(yunki.posX + (slideSprites[yunki.frame]:getWidth() / 2), yunki.posY - (slideSprites[yunki.frame]:getHeight() / 2))
          end
        end
      end
    end
  end
end

function advanceSlides(max)
  slideAdvanceSound:stop()
  slideAdvanceSound:play()
  if currentSlide < max then
    currentSlide = currentSlide + 1
    return false
  else
    currentSlide = 0
    return true
  end
end

function checkPositions(k)
  for i = 0, level, 1
  do
    if i ~= k then
      if kruncanites[i].position == kruncanites[k].position then
        return true
      end
    end
  end
  return false
end

function activateBoss()
  kCount = KREQUIRED
  for i = 0, level, 1
  do
    kruncanites[i]:deid()
  end
  for i = 0, 5, 1 do
    slideSprites[1] = null
  end
  slideSprites[0] = love.graphics.newImage("TEX/YUNKI1.png")
  slideSprites[1] = love.graphics.newImage("TEX/YUNKI2.png")
  brickSprite = love.graphics.newImage("TEX/SMOKE.png")
  boss_mode = true
  yunki.state = 1
end

function resetGame()
  alpha:respawn()
  boss_mode = false
  yunki.health = yunki_max_health
  alpha.health = 100
  kCount = 0
  level = 0
  loadScene(2)
end

function loadScene(scene)
  if scene == 0 then
    slideSprites[0] = love.graphics.newImage("TEX/SLIDES/LOGO.png")
  elseif scene == 1 then
    -- slideSprites[0] = love.graphics.newImage("TEX/slideSprites/1.png")
    slideSprites[0] = love.graphics.newImage("TEX/SLIDES/2.png")
    -- slideSprites[1] = love.graphics.newImage("TEX/slideSprites/3.png")
    -- slideSprites[2] = love.graphics.newImage("TEX/slideSprites/4.png")
    slideSprites[1] = love.graphics.newImage("TEX/SLIDES/5.png")
    slideSprites[2] = love.graphics.newImage("TEX/SLIDES/6.png")
    -- slideSprites[7] = love.graphics.newImage("TEX/slideSprites/7.png")
    slideSprites[3] = love.graphics.newImage("TEX/SLIDES/8.png")
    if language == 0 then
      setUpStartSlidesScots()
    else
      setUpStartSlides()
    end
  elseif scene == 3 then
    slideSprites[0] = love.graphics.newImage("TEX/SLIDES/9.png")
    if language == 0 then
      setUpEndSlidesScots()
    else
      setUpEndSlides()
    end
    -- slideSprites[1] = love.graphics.newImage("TEX/slideSprites/10.png")
  else
    generateQuotes()
    BG = love.graphics.newImage("TEX/BG.png")

    slideSprites[0] = love.graphics.newImage("TEX/KRN.png")
    slideSprites[1] = love.graphics.newImage("TEX/KRN2.png")
    -- slideSprites[2] = love.graphics.newImage("TEX/KRN3.png")
    -- slideSprites[3] = love.graphics.newImage("TEX/KRNF1.png")
    -- slideSprites[4] = love.graphics.newImage("TEX/KRNF2.png")
    
    brickSprite = love.graphics.newImage("TEX/BRICK.png")
    speechSprite = love.graphics.newImage("TEX/SB.png")

    slimeSprite = love.graphics.newImage("TEX/SLIME.png")

    shotgunSound = love.audio.newSource("Sound/SHOTGUN.wav")
    explosionSound = love.audio.newSource("Sound/BOOM.wav")
    slimeSound = love.audio.newSource("Sound/SLIME.wav")
  end
  gameMode = scene
end