Slide = {}

function Slide:create()
  local slide = setmetatable({}, Slide)
  self.__index = self
  self.slide = 0
  self.text1 = "Slide"
  self.text2 = "Slide"
  self.text3 = "Slide"
  self.text4 = "Slide"
  self.text5 = "Slide"

  return slide
end

function setUpStartSlides()
    slides[0] = Slide:create()
    slides[0].text1 = "Good Evening, Comrade. This is the general."
    slides[0].text2 = "We will be sending you to Covanmill,"
    slides[0].text3 = "Glasgow, Scotland for this mission."
    slides[0].text4 = ""
    slides[0].text5 = ""
    slides[0].slide = 0

    slides[1] = Slide:create()
    slides[1].text1 = "There is an abandoned college there,"
    slides[1].text2 = "which turns out not to be so abandoned."
    slides[1].text3 = ""
    slides[1].text4 = ""
    slides[1].text5 = ""
    slides[1].slide = 0

    slides[2] = Slide:create()
    slides[2].text1 = "The college is always packed with NEDs,"
    slides[2].text2 = "but upon closer inspection,"
    slides[2].text3 = "they're not your typical NEDs at all."
    slides[2].text4 = ""
    slides[2].text5 = ""
    slides[2].slide = 0

    slides[3] = Slide:create()
    slides[3].text1 = "These ones are completely identical,"
    slides[3].text2 = "and more come out of the college than go in."
    slides[3].text3 = "It turns out that the Kruncanites have"
    slides[3].text4 = "a cloning facility underneath the college."
    slides[3].text5 = ""
    slides[3].slide = 1

    slides[4] = Slide:create()
    slides[4].text1 = "When they were caught having a fierce fight, they all exploded,"
    slides[4].text2 = "and instead of blood, black slime came out."
    slides[4].text3 = "We took a sample back to base, and"
    slides[4].text4 = "none of it's chemical components"
    slides[4].text5 = "match anything seen here."
    slides[4].slide = 1

    slides[5] = Slide:create()
    slides[5].text1 = "They've been traced back to a recently discovered planet"
    slides[5].text2 = "named \"Kruncia\", the 9th planet in our solar system."
    slides[5].text3 = "The planet is so horrendously filthy and poluted"
    slides[5].text4 = "that we thought nobody could possible live on this planet."
    slides[5].text5 = "We were wrong."
    slides[5].slide = 1

    slides[6] = Slide:create()
    slides[6].text1 = "The earth's atmosphere must be so uninhabitable for them that"
    slides[6].text2 = "They use cigarettes as breathing masks."
    slides[6].text3 = "They speak a language sound like Glaswegian Scots"
    slides[6].text4 = "from a distance, but was actually an entirely"
    slides[6].text5 = "seperate language, which we are calling \"Kruncrarian\"."
    slides[6].slide = 1

    slides[7] = Slide:create()
    slides[7].text1 = "According to documents stolen from their HQ,"
    slides[7].text2 = "once everybody is under Kruncanite control,"
    slides[7].text3 = "they're going to turn the planet into a giant cigarette."
    slides[7].text4 = ""
    slides[7].text5 = ""
    slides[7].slide = 1

    slides[8] = Slide:create()
    slides[8].text1 = "...and this is the mastermind behind it all:"
    slides[8].text2 = "Emperor Yunki!"
    slides[8].text3 = ""
    slides[8].text4 = "The cigarette is part of a ceremony to decide the"
    slides[8].text5 = "new emperor."
    slides[8].slide = 2

    slides[9] = Slide:create()
    slides[9].text1 = "The Kruncanites will fight to the death to smoke"
    slides[9].text2 = "the giant Cigarette."
    slides[9].text3 = "The winner will become the new emperor."
    slides[9].text4 = "We must put a stop to this"
    slides[9].text5 = "and destroy Emperor Yunki!"
    slides[9].slide = 2

    slides[10] = Slide:create()
    slides[10].text1 = "We have given you a dart shotgun for this mission."
    slides[10].text2 = "Kruncanites should only take 1 hit to kill."
    slides[10].text3 = KREQUIRED .. " kruncanites should be enough to attract Yunki."
    slides[10].text4 = "We've giving you all of the info you need."
    slides[10].text5 = "Good luck comrade, you'll need it."
    slides[10].slide = 3
end

function setUpStartSlidesScots()
  slides[0] = Slide:create()
  slides[0].text1 = "Guid evenin, Comrade. 'Is is 'e general."
  slides[0].text2 = "We'll be firin ye intae Covanmill,"
  slides[0].text3 = "in Glesgae fir 'is meession."
  slides[0].text4 = ""
  slides[0].text5 = ""
  slides[0].slide = 0

  slides[1] = Slide:create()
  slides[1].text1 = "Thare's an abandont college thare, bit"
  slides[1].text2 = "hit turns oot the college isna that abbandont."
  slides[1].text3 = ""
  slides[1].text4 = ""
  slides[1].text5 = ""
  slides[1].slide = 0

  slides[2] = Slide:create()
  slides[2].text1 = "The college is aye pure hoachin wi NEDs,"
  slides[2].text2 = "bit whan ye leuk faurer intae hit,"
  slides[2].text3 = "thay're no yer teepical NEDs at aw."
  slides[2].text4 = ""
  slides[2].text5 = ""
  slides[2].slide = 0

  slides[3] = Slide:create()
  slides[3].text1 = "These yins ur tot the same,"
  slides[3].text2 = "an mair come oot ae 'e college'n git in."
  slides[3].text3 = "It tirns oot thit 'e Kruncanites hiv"
  slides[3].text4 = "a clonin faceelity unner 'e college."
  slides[3].text5 = ""
  slides[3].slide = 1

  slides[4] = Slide:create()
  slides[4].text1 = "Whan they were caucht batterin wan anither, thay aw explodtit,"
  slides[4].text2 = "an insteid ae bluid, black slime came ooot."
  slides[4].text3 = "We teuk a saumple back tae base, an"
  slides[4].text4 = "nane ae it's chemical components"
  slides[4].text5 = "match onyhing seen here."
  slides[4].slide = 1

  slides[5] = Slide:create()
  slides[5].text1 = "Thay'v bin tracit back tae ae planet"
  slides[5].text2 = "cawed \"Kruncia\", the 9t planet in wir solar seestem."
  slides[5].text3 = "The planet is that manky an polutit"
  slides[5].text4 = "thit we thocht thit naeb'dy coud possibly bide on 'is planet."
  slides[5].text5 = "We war wrang."
  slides[5].slide = 1

  slides[6] = Slide:create()
  slides[6].text1 = "The yird's airs maun be that uninhabitable fur thaim thit"
  slides[6].text2 = "Thay uise fags as breathin masks."
  slides[6].text3 = "Thay tawk a leed thit soonds like Glesgan Scots"
  slides[6].text4 = "fae ae distance, bit is actual a hale"
  slides[6].text5 = "different leed, which we'r cawin \"Kruncrarian\"."
  slides[6].slide = 1

  slides[7] = Slide:create()
  slides[7].text1 = "Bi the documents stealt fae thair HQ wey ae it,"
  slides[7].text2 = "wance thay hiv iverywan unner thair control,"
  slides[7].text3 = "thay'r gaun'ae tirn 'e planet intae a giant fag."
  slides[7].text4 = ""
  slides[7].text5 = ""
  slides[7].slide = 1

  slides[8] = Slide:create()
  slides[8].text1 = "...an 'is is the mastermind ahint aw ae it:"
  slides[8].text2 = "Emperor Yunki!"
  slides[8].text3 = ""
  slides[8].text4 = "The cigarette is pairt ae a ceremony tae decide the"
  slides[8].text5 = "new emperor."
  slides[8].slide = 2

  slides[9] = Slide:create()
  slides[9].text1 = "The Kruncanites'll ficht tae the deith tae smeuk"
  slides[9].text2 = "the giant Cigarette."
  slides[9].text3 = "The winner'll become the new emperor."
  slides[9].text4 = "We'v got'ae pit a stap tae 'is"
  slides[9].text5 = "an malafouster Emperor Yunki!"
  slides[9].slide = 2

  slides[10] = Slide:create()
  slides[10].text1 = "We'v' gien ye a dart shotgun fur 'is mission."
  slides[10].text2 = "Kruncanites shoud juist tak 1 hit tae kill."
  slides[10].text3 = KREQUIRED .. " kruncanite shoud be eneuch tae attract Yunki."
  slides[10].text4 = "We'v gien ye aw the info ye need."
  slides[10].text5 = "Guid luck comrade, ye'll be needin it."
  slides[10].slide = 3
end

function setUpEndSlides()
  slides[0] = Slide:create()
    slides[0].text1 = "After a ferocious battle with Emperor Yunki, he died..."
    slides[0].text2 = "...of cancer."
    slides[0].text3 = ""
    slides[0].text4 = ""
    slides[0].text5 = ""
    slides[0].slide = 0

    slides[1] = Slide:create()
    slides[1].text1 = "The world is a much better place now."
    slides[1].text2 = "The few Kruncanites left are being rounded up"
    slides[1].text3 = "and deported, and it's all thanks to you."
    slides[1].text4 = "Good job, Comrade! We salute you!"
    slides[1].text5 = ""
    slides[1].slide = 0
end

function setUpEndSlidesScots()
  slides[0] = Slide:create()
    slides[0].text1 = "Efter a ferocious fecht wi Emperor Yunki, he deed..."
    slides[0].text2 = "...ae cancer."
    slides[0].text3 = ""
    slides[0].text4 = ""
    slides[0].text5 = ""
    slides[0].slide = 0

    slides[1] = Slide:create()
    slides[1].text1 = "The warld's nou a much better place."
    slides[1].text2 = "The few Kruncanites left ur bein roondit up"
    slides[1].text3 = "an sent back, an hit's aw thanks tae yersel."
    slides[1].text4 = "Guid job, Comrade! We hailse you!"
    slides[1].text5 = ""
    slides[1].slide = 0
end