SB = {}
text1 = {}
text2 = {}
text3 = {}

local cooler = 1.5

function SB:create()
  local sb = setmetatable({}, SB)
  self.__index = self
  self.positionx = 0
  self.positiony = 20
  self.available = true
  self.timer = 0
  self.currentQuote = 0
  self.flipped = false

  self.sprite = love.graphics.newImage("TEX/SB.png")
  math.randomseed(os.time())

  return sb
end

function SB:sayRandom(posX)
    if posX > love.graphics.getWidth() / 2 then
        self.positionx = posX - 50
        self.flipped = true
    else
        self.positionx = posX + 40
        self.flipped = false
    end
    self.currentQuote = math.random(8)
    self.available = false
    self.timer = 0
end

function SB:say(posX, quote)
    if posX > love.graphics.getWidth() / 2 then
        self.positionx = posX - 50
    else
        self.positionx = posX + 40
    end
    self.currentQuote = quote
    self.available = false
    self.timer = 0
end

function SB:process()
    if self.timer < cooler then
        self.timer = self.timer + love.timer.getDelta()
    else
        self.available = true
    end
end

function generateQuotes()
  text1[0] = ""
  text2[0] = "Gies a fag!"
  text3[0] = ""

  text1[1] = ""
  text2[1] = "Bawbag!"
  text3[1] = ""

  text1[2] = ""
  text2[2] = "S'happnin!"
  text3[2] = ""

  text1[3] = "A'm gaun'ae"
  text2[3] = "Hump You!"
  text3[3] = ""

  text1[4] = "A'm gaun'ae"
  text2[4] = "Boot ye!"
  text3[4] = ""

  text1[5] = ""
  text2[5] = "A'll boot ye!"
  text3[5] = ""

  text1[6] = ""
  text2[6] = "A'll crack ye!"
  text3[6] = ""

  text1[7] = ""
  text2[7] = "Ya bam, you!"
  text3[7] = ""

  text1[8] = "A'm gaun'ae"
  text2[8] = "rip yer"
  text3[8] = "jaw!"
  
  text1[9] = "Success..."
  text2[9] = "...um..."
  text3[9] = "...um..."

  text1[10] = "Success..."
  text2[10] = "...ow..."
  text3[10] = "...ow..."
end