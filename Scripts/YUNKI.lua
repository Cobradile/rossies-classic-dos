Yunki = {}
local cooler = 0.01
death_cooler = 5
local smoke_cooler = 1
yunki_max_health = 100

function Yunki:create()
  local yunki = setmetatable({}, Yunki)
  self.__index = self
  self.posX = (love.graphics.getWidth() / 2)
  self.posY = (love.graphics.getHeight() / 2)
  self.health = yunki_max_health
  self.state = 0
  self.frame = 0
  self.timer = 0
  self.timer2 = 0
  self.targetX = 0
  self.targetY = 0
  self.nextTargetX = 0
  self.nextTargetY = 0
  self.attackCount = 0


  return yunki
end

function Yunki:spawn()
  speechBubble:say(self.posX, 9)
end

function Yunki:hurt()
  speechBubble:say(self.posX, 10)
  self.health = self.health - 10
  self.attackCount = 0
  self.state = 2
  self.frame = 1
  if self.health > 0 then
    self.targetX = (love.graphics.getWidth() / 2)
    self.targetY = (love.graphics.getHeight() / 2)
  else
    self.frame = 0
    self:die()
  end
end

function Yunki:die()
  self.state = 0
end

function Yunki:update()
  if self.timer < cooler then
    self.timer = self.timer + love.timer.getDelta()
    if self.state <= 1 then
      if math.abs(self.nextTargetX) - math.abs(self.targetX) < 50 then
        self.nextTargetX = math.random(love.graphics.getWidth())
      end
      if math.abs(self.nextTargetY) - math.abs(self.targetY) < 50 then
        self.nextTargetY = math.random(love.graphics.getHeight() / 2) + (love.graphics.getHeight() / 3)
      end
    end
  elseif self.state == 3 then
    brick:fling(self.posX, alpha.position)
    self.attackCount = self.attackCount + 1
    self.timer = 0
    if self.attackCount >= 3 then
      self.attackCount = 0
      self.state = 1
      cooler = 0.01
    end
  elseif self.state >= 0 then
      if self.posX < self.targetX - 10 then
          self.posX = self.posX + 5
      elseif self.posX > self.targetX + 10 then
        self.posX = self.posX - 5
      elseif self.state <= 1 then
        self.targetX = self.nextTargetX
        math.randomseed(os.time())
      end
      
      if self.posY < self.targetY - 10 then
          self.posY = self.posY + 5
      elseif self.posY > self.targetY + 10 then
        self.posY = self.posY - 5
      elseif self.state <= 1 then
        self.targetY = self.nextTargetY
        math.randomseed(os.time())
      elseif self.state == 2 then
        cooler = 3
      end
      self.timer = 0
  end

  if self.state == 2 then
    if self.timer2 < smoke_cooler then
      self.timer2 = self.timer2 + love.timer.getDelta()
    else
      self.timer2 = 0
      self.frame = 0
      self.state = 3
    end
  elseif self.state == 0 then
    if self.timer2 < death_cooler then
      self.timer2 = self.timer2 + love.timer.getDelta()
    else
      self.state = -1
      self.timer2 = 0
      loadScene(3)
    end
  end
end