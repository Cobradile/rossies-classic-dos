Alpha = {}

local animCooler = 0.15

function Alpha:create()
  local alpha = setmetatable({}, Alpha)
  self.__index = self
  self.position = 0
  self.state = 1

  self.timer = 0

  self.health = 10

  self.frame = 0
  self.anim = {}
  self.anim[0] = love.graphics.newImage("TEX/GUN1.png")
  self.anim[1] = love.graphics.newImage("TEX/GUN2.png")
  self.anim[2] = love.graphics.newImage("TEX/GUN3.png")
  self.anim[3] = self.anim[1]
  self.anim[4] = love.graphics.newImage("TEX/GUN4.png")
  self.anim[5] = love.graphics.newImage("TEX/GUN5.png")
  self.anim[6] = love.graphics.newImage("TEX/GUNSIX.png")
  self.anim[7] = self.anim[5]
  self.anim[8] = self.anim[4]

  self.y = {}

  return alpha
end

function Alpha:update()
  if self.state > 0 then
    if love.keyboard.isDown("left") then
      if self.position > -30 then
        self.position = self.position - 300 * love.timer.getDelta()
      end
    elseif love.keyboard.isDown("right") then
      if self.position < 260 then
        self.position = self.position + 300 * love.timer.getDelta()
      end
    end
  end
  
  if self.state == 2 then
    if self.timer < animCooler then
      self.timer = self.timer + love.timer.getDelta()
    elseif alpha.frame < 8 then
      self.frame = self.frame + 1
      self.timer = 0
    else
      self.state = 1
      self.frame = 0
      self.timer = 0
    end
  elseif self.state == 0 then
    if self.y[self.frame] > -80 then
      if self.timer < animCooler then
        self.timer = self.timer + love.timer.getDelta()
      else
        self.y[self.frame] = self.y[self.frame] + 5
      end
    end
  end
end

function Alpha:respawn()
  self.state = 1
  self.frame = 0
  self.timer = 0
  self.position = (love.graphics.getWidth() / 2) - (self.anim[0]:getWidth() / 2)
  self.y[0] = 137
  self.y[1] = 132
  self.y[2] = 110
  self.y[3] = self.y[1]
  self.y[4] = 137
  self.y[5] = 70
  self.y[6] = 80
  self.y[7] = self.y[5]
  self.y[8] = self.y[4]
end

function Alpha:changeHealth(amoont)
  self.health = self.health - amoont
  if self.health <= 0 then
    self.state = 0
  end
end