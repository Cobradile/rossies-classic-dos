Kruncanite = {}
function Kruncanite:create()
  local krn = setmetatable({}, Kruncanite)
  self.__index = self
  self.position = 0
  self.state = 0
  self.frame = 0
  self.timer = 0
  self.currentQuote = 0
  self.cooler = kCooler

  return krn
end

function Kruncanite:spawn()
  if self.timer < self.cooler then
    self.timer = self.timer + love.timer.getDelta()
  else
    self.frame = 0
    self.timer = 0
    self.state = 1
    self.position = math.random(260)
    self.cooler = math.random(kCooler)
    speechBubble:sayRandom(self.position)
  end
end

function Kruncanite:logic()
  if self.timer < self.cooler then
    self.timer = self.timer + love.timer.getDelta()
  elseif self.state == 1 then
    self.state = 2
    self.frame = 1
    self.timer = 0
    self.cooler = 0.3
    brick:fling(self.position, alpha.position)
  elseif self.state == 2 then
    self.frame = 0
    self.state = 1
    self.cooler = 2
  elseif self.state == 3 then
    self.state = 0
    self.timer = 0
    self.cooler = math.random(kCooler * 2)
  end
end

function Kruncanite:deid()
  -- self.frame = 2
  self.state = 3
  self.timer = 0
  self.cooler = 0.2
  self.position = self.position - 90
end