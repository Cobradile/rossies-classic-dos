Brick = {}

local cooler = 0.005

function Brick:create()
  local brck = setmetatable({}, Brick)
  self.__index = self
  self.positionx = 0
  self.positiony = 100
  self.velocityx = 0
  self.velocityy = 0
  self.targetx = 0
  self.targety = 200
  self.rotation = 0
  self.available = true
  self.timer = 0

  return brck
end

function Brick:fling(from, to)
    self.positionx = from
    self.positiony = 100

    self.targetx = to
    self.velocityx = ((self.targetx - self.positionx) / 70);
	self.velocityy = (math.abs(self.velocityx / 10)) + 1;

    self.available = false
end

function Brick:move()
    if self.timer < cooler then
        self.timer = self.timer + love.timer.getDelta()
    else
        if self.positionx ~= self.targetx then
            self.positionx = self.positionx + self.velocityx
        end
        
        if self.positiony < self.targety then
            self.positiony = self.positiony + self.velocityy
        else
            self.available = true
        end
        self.timer = 0
    end

    if collide2d(alpha.position, alpha.y[alpha.frame], 20, 30, self.positionx, self.positiony, 10, 10) then
        alpha:changeHealth(10)
        self.available = true
    end
end